import re
import requests
import schedule
import time
##---------->pip install schedule<-----im terminal--------###
username = ""
passwort = ""

moodledomain = "https://lernplattform.gfn.de/"
loginpath = "/login/index.php"
logoutpath = "/logout/index.php"

def main():
    mysession = requests.session()
    r1 = mysession.get(url=moodledomain+loginpath)
    haystack = r1.text

    regex = '"logintoken" value="([^"]+)"'
    logintoken_match = re.search(regex, haystack)

    if logintoken_match:
        logintoken = logintoken_match.group(1)
        print("Token found:", logintoken)
        data = {
            'anchor': '',
            'logintoken': logintoken,
            'username': "xxxx-hier user-xxxx",
            'password': "xxx-hier password-xxx"
        }
        r2 = mysession.post(url=moodledomain+loginpath, data=data)
        haystack = r2.text

        regex = "Ungültige Anmeldedaten. Versuchen Sie es noch einmal!"
        fail_string = re.search(regex, haystack)

        if not fail_string:
            print("Login Successful")
            r3 = mysession.get(moodledomain, params={"stoppen": 1})
            print(r3.text)
        else:
            print("Login failed")
    else:
        print("Token not found")

def logout():
    mysession = requests.session()
    r = mysession.get(url=moodledomain+logoutpath)
    print("Logged out successfully")

# Schedule den Login um 8:00 Uhr von Montag bis Freitag
schedule.every().monday.at("08:00").do(main)
schedule.every().tuesday.at("08:00").do(main)
schedule.every().wednesday.at("08:00").do(main)
schedule.every().thursday.at("08:00").do(main)
schedule.every().friday.at("08:00").do(main)

# Schedule den Logout um 16:35 Uhr von Montag bis Freitag
schedule.every().monday.at("16:35").do(logout)
schedule.every().tuesday.at("16:35").do(logout)
schedule.every().wednesday.at("16:35").do(logout)
schedule.every().thursday.at("16:35").do(logout)
schedule.every().friday.at("16:35").do(logout)

# Dauerhaft die Schedule-Aufgaben ausführen
while True:
    schedule.run_pending()
    time.sleep(1)
